//ES6+ ECMAN 2015 - ES6
// var -> let e const

// Var - Escopo de função, deixa sobrescrever uma variável.
// Let - Escopo de Bloco, não deixa sobrescrever.
let config = "Uma configuração Global";
// 100 linhas de código
config = "Uma simples configuração local";

console.log("config :>> ", config);

// Var - Vazamento de dados - Sobrecarga de memória
let status = "default";
if (config.length < 10) {
  status = "Acesso permitido";
}

console.log("status :>> ", status);

// const
let nameUser = "Angelo";
//... vários códigos loucos

nameUser = "Pedro";

const user = { name: "Angelo", yearBirth: "1986", type: "W" }; //Objeto

user.name = "Gabriel";
// user = { name: "Gabriel", yearBirth: "1986", type: "W" };
console.log("user :>> ", user); //Funciona? -+

const user2 = { ...user };
console.log("user2 :>> ", user2); //Funciona? ++

user2.name = "Verônica";
user.yearBirth = "1980";

console.log("user2-- :>> ", user2); // O que sai?
console.log("user-- :>> ", user); // O que sai?

//      |  { name: "Verônica", yearBirth: "1986", type: "W" }  |  { name: "Verônica", yearBirth: "1986", type: "W" }    |     |      |      |       |      | ...... |       |
//                   ^                                                  ^
//                  user                                             user2
