import { faker } from "@faker-js/faker";

// 100 Clientes
// name, birthday, genre, lastPurchaseDate, countPurchase

/*
 * PREMISSSAS:
 * - [x] 100 Clientes
 * - [x] Os dados devem estar em pt_BR;
 * - [x] Os dados devem ser coerentes com os seus campos. Ex: é impossível que algum cliente tenha nascido em 2022;
 * - [x] As datas de nascimento devem ser entre 1910 a 2010.
 * - [x] Os nomes devem ser nomes completos.
 */
const clients = [];
faker.setLocale("pt_BR");
const clientsAmount = (amount) => {
  for (let index = 0; index < amount; index++) {
    const client = {
      name: faker.name.findName(),
      birthday: faker.date.between("1910", "2010"),
      gender: faker.name.gender(true),
      lastPurchaseDate: faker.date.between("2018", "2022"),
      countPurchase: faker.datatype.number(),
    };
    clients.push(client);
  }
};

clientsAmount(100);
// console.log(clients);
//console.log(clients.length);

//faker.name.gender(true); => Boolean...

/*
 * QUEST 1: Desenvolva, utilizando filter , uma função que,
 * dado um caractere de entrada, retorne todos os registros de
 * clientes cujo o nome inicia com o caractere.
 */
const filteredClientsByFirstChar = (letter) =>
  clients.filter((client) => client.name.startsWith(letter));

// console.log(filteredClientsByFirstChar("B"));

/*
 * QUEST 2: Liste os nomes dos clientes no padrão: "Cliente: NOME_DO_CLIENTE"
 */
const clientsNames = () =>
  clients.forEach((client) => console.log(`Cliente: ${client.name}`));

/*
 * QUEST 3: Liste os nomes dos clientes no padrão: "Cliente INDEX: NOME_DO_CLIENTE".
 */
const clientsNamesindex = () => {
  clients.forEach((client, index) =>
    console.log(`Cliente ${++index}: ${client.name}`)
  );
};

// clientsNamesindex();

/**
 * QUEST 4
 * Desenvolva uma função que, dado o caractere de entrada,
 * retorna apenas um número com o total de registros encontrados de clientes que
 * iniciam com o caractere recebido.
 */

const totalClientsByFirstChar = (letter) =>
  clients.filter((client) => client.name.startsWith(letter)).length;

console.log("TOTAL: ", totalClientsByFirstChar("A"));

/**
 * QUEST 5: Desenvolva uma função que retorne apenas o primeiro nome dos clientes.
 */
const firstNameClients = () =>
  clients.map((client) => client.name.split(" ")[0]);

//console.log("firstNameClients() :>> ", firstNameClients());

/**
 * QUEST 6
 * Desenvolva uma função que retorne apenas o primeiro nome dos clientes cujo
 * os nomes começam com o caractere de entrada da função.
 */
const filteredClientsByFirstChar2 = (letter) =>
  // firstNameClients().filter((client) => client.startsWith(letter));
  clients
    .filter((client) => client.name.startsWith(letter))
    .map((filteredClient) => filteredClient.name.split(" ")[0]);

console.log(filteredClientsByFirstChar2("A"));

/**
 * QUEST 7: Implemente uma função que retorne os dados dos clientes que não
 * compram há mais de 1 ano.
 */

const filteredClientsByDatePurchase = () => {
  const oneMinuteInMillis = 1000 * 60;
  const oneHourInMillis = oneMinuteInMillis * 60;
  const oneYearInMillis = oneHourInMillis * 24 * 365;
  return clients.filter(
    (client) =>
      client.lastPurchaseDate.getTime() + oneYearInMillis < new Date().getTime()
  );
};

console.log(filteredClientsByDatePurchase());
