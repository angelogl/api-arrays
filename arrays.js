import { faker } from "@faker-js/faker";

faker.setLocale("pt_BR");

const users = [];

// Geração de dados falsos
const fake = (amount) => {
  for (let index = 0; index < amount; index++) {
    users.push(faker.name.firstName());
  }
};

fake(20);

// for (let index = 0; index < users.length; index++) {
//   const names = (faker.names.findName("A"))

// }

const names2 = [];
for (let index = 0; index < users.length; index++) {
  if (users[index].startsWith("A")) {
    names2.push(users[index]);
  }
}
// console.log(names2);
// valor => CONDIÇÃO
const filteredUsers = users.filter((user) => user.startsWith("A"));
console.log("filteredUsers :>> ", filteredUsers);

//Adicione "Silva" como sobrenome de todos com laço

for (let index = 0; index < users.length; index++) {
  users[index] += " Silva";
}

//Adicione "Silva" como sobrenome de todos com map
const mappedUsers = users.map((user) => (user += " Silva"));
console.log("mappedUsers :>> ", mappedUsers);
