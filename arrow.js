const users = ["Gabriel", "Pedro", "Verônica"];

function list() {
  return users;
}

const list2 = () => users;

// const list2 = () => {
//   return users;
// };

console.log("list2 :>> ", list2());

// Função de salvar
function save(user) {
  users.push(user);
}

// Função de salvar com arrow function
const save2 = (user) => users.push(user);

save2("Angelo");
console.log("users :>> ", users);
